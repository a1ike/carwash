$(document).ready(function () {
  $('.phone').inputmask({
    mask: '+7(999)-999-99-99',
    showMaskOnHover: false,
  });

  $('.r-header__mob').on('click', function (e) {
    e.preventDefault();

    $('.r-header__info').slideToggle('fast');
    $('.r-header__right').slideToggle('fast');
    $('.r-nav').slideToggle('fast');
  });

  $('.open-modal').on('click', function (e) {
    e.preventDefault();

    $('.r-modal').toggle();
  });

  $('.r-modal__centered').on('click', function (e) {
    e.preventDefault();

    if (e.target.className === 'r-modal__centered') {
      $('.r-modal').hide();
    }
  });

  $('.r-modal__close').on('click', function (e) {
    e.preventDefault();
    $('.r-modal').hide();
  });

  $('.r-view__icon').on('click', function (e) {
    e.preventDefault();

    $('.r-view__description').hide();
    $(this).next().show();
  });

  $('.r-view__description').on('click', function (e) {
    e.preventDefault();

    $(this).hide();
  });

  new Swiper('.r-home-done__cards', {
    navigation: {
      nextEl: '.r-home-done__cards .swiper-button-next',
      prevEl: '.r-home-done__cards .swiper-button-prev',
    },
    spaceBetween: 300,
    loop: true,
    /* autoplay: {
      delay: 5000,
    }, */
  });

  new Swiper('.r-home-reviews__cards', {
    navigation: {
      nextEl: '.r-home-reviews__cards .swiper-button-next',
      prevEl: '.r-home-reviews__cards .swiper-button-prev',
    },
    loop: true,
    spaceBetween: 45,
    slidesPerView: 1,
    breakpoints: {
      1024: {
        slidesPerView: 2,
      },
      1200: {
        slidesPerView: 3,
      },
    },
  });

  new Swiper('.r-papers__cards', {
    navigation: {
      nextEl: '.r-papers .swiper-button-next',
      prevEl: '.r-papers .swiper-button-prev',
    },
    pagination: {
      el: '.r-papers .swiper-pagination',
    },
    loop: true,
    spaceBetween: 40,
    slidesPerView: 1,
    breakpoints: {
      1024: {
        slidesPerView: 3,
      },
      1200: {
        slidesPerView: 5,
      },
    },
  });

  new Swiper('.r-project-photo__cards', {
    navigation: {
      nextEl: '.r-project-photo__cards .swiper-button-next',
      prevEl: '.r-project-photo__cards .swiper-button-prev',
    },
    spaceBetween: 70,
    loop: true,
    /* autoplay: {
      delay: 5000,
    }, */
  });

  new Swiper('.r-features__cards', {
    navigation: {
      nextEl: '.r-features__cards .swiper-button-next',
      prevEl: '.r-features__cards .swiper-button-prev',
    },
    pagination: {
      el: '.r-features__cards .swiper-pagination',
    },
    loop: true,
    spaceBetween: 45,
    slidesPerView: 1,
    breakpoints: {
      1024: {
        slidesPerView: 2,
      },
      1200: {
        slidesPerView: 3,
      },
    },
  });
});
